# Prediction using centroid distance


__author__ = "Washief Hossain"

import csv
import math
import random
import numpy as np


with open('generated_dataset/gaze_dataset.csv') as gazedata:
    gazereader = csv.DictReader(gazedata)
    gazes = list(gazereader)


def split_dataset(dataset, ratio):
    assert(ratio > 0 and ratio < 1)
    random.shuffle(dataset)
    train_size = int(len(dataset)*ratio)
    train_set = dataset[:train_size]
    test_set = dataset[train_size:]
    return (train_set, test_set)


def get_centroid(data_set):
    return {'blinks/duration': np.average([float(row['blinks/duration']) for row in data_set]),
            'avg_pupil': np.average([float(row['avg_pupil']) for row in data_set]),
            'sd_pupil': np.average([float(row['sd_pupil']) for row in data_set]),
            'fixations': np.average([float(row['fixations']) for row in data_set])}


def getDistance(point1, point2):
    return math.sqrt(pow(float(point1['blinks/duration']) - float(point2['blinks/duration']), 2) +
                     pow(float(point1['sd_pupil']) - float(point2['sd_pupil']), 2))  # performance improves if we only consider blinks/duration and sd_pupil


def test(test_set, true_centroid, false_centroid):
    correct = 0
    for sample_test in test_set:
        true_dist = getDistance(sample_test, true_centroid)
        false_dist = getDistance(sample_test, false_centroid)
        verdict = '0'
        if true_dist <= false_dist:
            verdict = '1'
        if verdict == sample_test['truth']:
            correct = correct + 1

    # print("Prediction correctness: ", (1.0*correct/len(test_set)))
    return 1.0*correct/len(test_set)


avg_accuracy = 0.0
iter = 1000
for i in range(iter):
    train_set, test_set = split_dataset(gazes, .75)
    true_samples = [row for row in train_set if row['truth'] == '1']
    false_samples = [row for row in train_set if row['truth'] != '1']
    avg_accuracy = (
        avg_accuracy*i +
        test(test_set, get_centroid(true_samples), get_centroid(false_samples))
    ) / (i+1)

print("Average accuracy: ", avg_accuracy*100.0, "percent")
