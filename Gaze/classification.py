# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv


#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE


#DIR = os.path.dirname(__file__)

# Import dataset:
    
dataset = pd.read_csv("./generated_dataset/gaze_dataset.csv")




# Assign column names to dataset:
#names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# Convert dataset to a pandas dataframe:
#dataset = pd.read_csv(url, names=names) 

# Use head() function to return the first 5 rows: 
dataset.head() 
# Assign values to the X and y variables:
X = dataset.iloc[:, 0:64].values
y = dataset.iloc[:, 64].values 

# Split dataset into random train and test subsets:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20) 

# Standardize features by removing mean and scaling to unit variance:
scaler = StandardScaler()
scaler.fit(X_train)

X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test) 

#Feature importance

# feature extraction
#Univariate Selection
test = SelectKBest(score_func=f_classif, k=50)
fit = test.fit(X_train, y_train)
# summarize scores
set_printoptions(precision=3)
print(fit.scores_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
# summarize selected features
#print(features[0:5,:])






#SVM Classification

from sklearn.svm import LinearSVC
svm=LinearSVC(C=0.0001)
rfe = RFE(svm, 64)
fit = rfe.fit(X, y)
print("Num Features: %d" % fit.n_features_)
print("Selected Features: %s" % fit.support_)
print("Feature Ranking: %s" % fit.ranking_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
svm.fit(X_train_features, y_train)
# Predict y data with classifier: 
y_predict = svm.predict(X_test_features)
print("SVM Classification Score Analysis")
accuracy_score(y_test, y_predict)



# Use the KNN classifier to fit data:
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train_features, y_train) 

# Predict y data with classifier: 
y_predict = classifier.predict(X_test_features)

#print("train shape: " + str(X_train.shape))
#print("score on test: " + str(classifier.score(X_test, y_predict)))
#print("score on train: "+ str(classifier.score(X_train, y_train)))
print("KNN Classification Score Analysis")
accuracy_score(y_test, y_predict)


#Decision Tree Classifcation

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(X_train_features, y_train)
# Predict y data with classifier: 
y_predict = clf.predict(X_test_features)
accuracy_score(y_test, y_predict)




# Print results: 
#print(confusion_matrix(y_test, y_predict))
#print(classification_report(y_test, y_predict)) 



