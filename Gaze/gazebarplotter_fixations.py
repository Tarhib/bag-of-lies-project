# Bar plot of overall dataset

__author__ = "Washief Hossain"

import os
import csv
import matplotlib.pyplot as plt
import numpy as np

DIR = os.path.dirname(__file__)


with open('generated_dataset/gaze_dataset.csv') as gazedata:
    gazereader = csv.DictReader(gazedata, quoting=csv.QUOTE_NONNUMERIC)
    gazes = list(gazereader)


true_samples = [row for row in gazes if row['truth'] == '1']
false_samples = [row for row in gazes if row['truth'] != '1']
true_single_sample= true_samples[0];
false_single_sample= false_samples[0];
true_sample_data=[]
false_sample_data=[]
for i in range(1,20):
    true_sample_data.append( true_single_sample["sample"+str(i)+"_d"])
    false_sample_data.append( false_single_sample["sample"+str(i)+"_d"])


plt.bar(range(0, len(true_sample_data)),
        true_sample_data, color='green', label='true')
plt.bar(range(len(true_sample_data), len(true_sample_data)+len(false_sample_data)),
        false_sample_data, color='red', label='false')

# plt.title(pretty_name[attribute].capitalize())
plt.ylabel("duration data")
plt.xlabel('subjects')
plt.xticks([])
plt.legend(prop={'size': 6})
if not os.path.exists('plots'):
    os.makedirs('plots')
plt.savefig(
    'plots/bar_{}.png'.format("duration"))
plt.close()


attributes = ['blinks/duration', 'avg_pupil', 'sd_pupil', 'fixations']
pretty_name = {'blinks/duration': 'blinking rate (Hz)', 'avg_pupil': 'average pupil size (in pixels)',
               'sd_pupil': 'standard deviation of pupil (in pixels)', 'fixations': 'number of fixations'}
# TODO: reuse attributes array instead of using name of attributes


def centroid(data_set):
    ret = {}
    for attr in attributes:
        ret[attr] = np.average([row[attr] for row in data_set])
    return ret


def data_tuple(data):
    return tuple([data[attribute] for attribute in attributes])


def distance(point1, point2):
    return np.linalg.norm(np.subtract(data_tuple(point1), data_tuple(point2)))


true_centroid = centroid(true_samples)
false_centroid = centroid(false_samples)

true_samples_filtered = [row for row in true_samples if distance(
    row, true_centroid) < distance(row, false_centroid)]

false_samples_filtered = [row for row in false_samples if distance(
    row, true_centroid) > distance(row, false_centroid)]


def plot_bar(attribute):
    true_attribute_samples = [row[attribute]
                              for row in true_samples_filtered]
    false_attribute_samples = [row[attribute]
                               for row in false_samples_filtered]

    true_average = np.average(true_attribute_samples)
    false_average = np.average(false_attribute_samples)

    plt.bar(range(0, len(true_attribute_samples)),
            true_attribute_samples, color='green', label='true')
    plt.bar(range(len(true_attribute_samples), len(true_attribute_samples)+len(false_attribute_samples)),
            false_attribute_samples, color='red', label='false')
    plt.plot(range(0, len(true_attribute_samples)), [
             true_average]*len(true_attribute_samples), color='black', label='average')
    plt.plot(range(len(true_attribute_samples), len(true_attribute_samples)+len(false_attribute_samples)),
             [false_average]*len(false_attribute_samples), color='black')
    # plt.title(pretty_name[attribute].capitalize())
    plt.ylabel(pretty_name[attribute])
    plt.xlabel('subjects')
    plt.xticks([])
    plt.legend(prop={'size': 6})
    if not os.path.exists('plots'):
        os.makedirs('plots')
    plt.savefig(
        'plots/bar_{}.png'.format(attribute.replace('/', '_by_')))
    plt.close()


for attribute in attributes:
    plot_bar(attribute)
