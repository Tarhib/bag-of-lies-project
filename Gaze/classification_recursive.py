# -*- coding: utf-8 -*-

from numpy import mean
from numpy import std
from sklearn.datasets import make_classification
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.feature_selection import RFE
from sklearn.tree import DecisionTreeClassifier
from sklearn.pipeline import Pipeline
import pandas as pd 
from sklearn.neighbors import KNeighborsClassifier

# Import dataset:
from sklearn.svm import LinearSVC
dataset = pd.read_csv("./generated_dataset/gaze_dataset.csv")

# Use head() function to return the first 5 rows: 
dataset.head() 
# Assign values to the X and y variables:
X = dataset.iloc[:, 0:64].values
y = dataset.iloc[:, 64].values 


# create pipeline
rfe = RFE(estimator=LinearSVC(C=0.0001), n_features_to_select=18)
model=KNeighborsClassifier(n_neighbors=5)

pipeline = Pipeline(steps=[('s',rfe),('m',model)])
# evaluate model
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
n_scores = cross_val_score(pipeline, X, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
# report performance
print("KNN Classification Score Analysis")

print('Accuracy: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))





# create pipeline
rfe = RFE(estimator=LinearSVC(C=0.0001), n_features_to_select=45)
model=LinearSVC(C=0.0001)

pipeline = Pipeline(steps=[('s',rfe),('m',model)])
# evaluate model
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
n_scores = cross_val_score(pipeline, X, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
# report performance
print("SVM Classification Score Analysis")

print('Accuracy: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))


# create pipeline
rfe = RFE(estimator=DecisionTreeClassifier(), n_features_to_select=45)
model=DecisionTreeClassifier()

pipeline = Pipeline(steps=[('s',rfe),('m',model)])
# evaluate model
cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
n_scores = cross_val_score(pipeline, X, y, scoring='accuracy', cv=cv, n_jobs=-1, error_score='raise')
# report performance
print("Decision Tree Score Analysis")
print('Accuracy: %.3f (%.3f)' % (mean(n_scores), std(n_scores)))






# Print results: 
#print(confusion_matrix(y_test, y_predict))
#print(classification_report(y_test, y_predict)) 



