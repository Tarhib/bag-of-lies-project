# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv
import numpy as np
from sklearn.decomposition import PCA
#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE
from grouped_permutation_importance import grouped_permutation_importance
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.svm import SVC
from sklearn.datasets import make_classification
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import Pipeline
#DIR = os.path.dirname(__file__)

# Import dataset:
    
dataset = pd.read_csv("./generated_dataset/gaze_dataset.csv")




# Assign column names to dataset:
#names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# Convert dataset to a pandas dataframe:
#dataset = pd.read_csv(url, names=names) 

# Use head() function to return the first 5 rows: 
dataset.head() 
# Assign values to the X and y variables:
X = dataset.iloc[:, 0:64].values
y = dataset.iloc[:, 64].values 

# Split dataset into random train and test subsets:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20) 

# Standardize features by removing mean and scaling to unit variance:
#scaler = StandardScaler()
#scaler.fit(X_train)

#X_train = scaler.transform(X_train)
#X_test = scaler.transform(X_test) 




# Get the list of all column names from headers
feature_names = list(dataset.columns.values)[:-3]



# different grouping
columns = ["sample0_sample1","sample2_sample3","sample4_sample5",
           "sample6_sample7","sample8_sample9","sample10_sample11","sample12_sample13",
           "sample14_sample15","sample16_sample17","sample18_sample19" ]

idxs = []
for key in columns:
    keys_element= key.split('_');
    idxs.append([x for (x, y) in enumerate(feature_names) if y[:-2] in keys_element])

#idxs.appned([])
#idxs = []


cv = RepeatedStratifiedKFold()
pipe = Pipeline([("MinMax", MinMaxScaler()),  ("SVC", SVC())])


r = grouped_permutation_importance(pipe, X, y, idxs=idxs, n_repeats=50, random_state=0, 
                                   scoring="balanced_accuracy", n_jobs=5, cv=cv, 
 
                           perm_set="train")

sorted_idx = r.importances_mean.argsort()[::-1]
X_Reduced=dataset.loc[:,"sample0_x"]
for index in range(7):
    column_val=columns[index].split("_");
    print(column_val)
    #X_Reduced = dataset.loc[:,column_val[0]+"_x"]
    #X_Reduced = X_Reduced.extend(dataset.loc[:,column_val[1]+"_x"])
    X_Reduced=pd.concat([X_Reduced,dataset.loc[:,column_val[0]+"_x"],dataset.loc[:,column_val[0]+"_y"],
                  dataset.loc[:,column_val[0]+"_d"]],axis=1)
    #X_Reduced=df.iloc[:,:]
    X_Reduced=pd.concat([X_Reduced,dataset.loc[:,column_val[1]+"_x"],dataset.loc[:,column_val[1]+"_y"],
                  dataset.loc[:,column_val[1]+"_d"]],axis=1)
    #X_Reduced=df.iloc[:,:]
   # X_Reduced.append(dataset[column_val[0]+"_x"].to)
    print(columns[index]);

X_Reduced=pd.concat([X_Reduced,dataset.loc[:,"blinks/duration"],dataset.loc[:,"avg_pupil"],
            dataset.loc[:,"sd_pupil"],  dataset.loc[:,"fixations"]],axis=1)
test=r.importances[sorted_idx].T

# Split dataset into random train and test subsets:
X_train, X_test, y_train, y_test = train_test_split(X_Reduced, y, test_size=0.20) 


#Feature importance

# feature extraction
#Univariate Selection
test = SelectKBest(score_func=f_classif, k=40)
fit = test.fit(X_train, y_train)
# summarize scores
set_printoptions(precision=3)
print(fit.scores_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
# summarize selected features
#print(features[0:5,:])



# Use the KNN classifier to fit data:
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train_features, y_train) 
# Predict y data with classifier: 
y_predict = classifier.predict(X_test_features)
#print("train shape: " + str(X_train.shape))
#print("score on test: " + str(classifier.score(X_test, y_predict)))
#print("score on train: "+ str(classifier.score(X_train, y_train)))
print("KNN Classification Score Analysis using Select K best")
knn_accuracy= accuracy_score(y_test, y_predict)
print(knn_accuracy)


#Feature importance

# feature extraction
#Univariate Selection
# feature extraction
pca = PCA(n_components=40)
fit = pca.fit(X_Reduced)
# summarize components
#print("Explained Variance: %s" % fit.explained_variance_ratio_)
#print(fit.components_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
# summarize selected features
#print(features[0:5,:])


# Use the KNN classifier to fit data:
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train_features, y_train) 
# Predict y data with classifier: 
y_predict = classifier.predict(X_test_features)
#print("train shape: " + str(X_train.shape))
#print("score on test: " + str(classifier.score(X_test, y_predict)))
#print("score on train: "+ str(classifier.score(X_train, y_train)))
print("KNN Classification Score Analysis using PCA")
knn_accuracy= accuracy_score(y_test, y_predict)
print(knn_accuracy)
