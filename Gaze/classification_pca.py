# -*- coding: utf-8 -*-

# Feature Extraction with PCA
import numpy
from pandas import read_csv
from sklearn.decomposition import PCA



# Import libraries and classes required for this example:
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler 
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.metrics import accuracy_score
import pandas as pd 
# native
import os
import csv

from sklearn.decomposition import PCA

#feature selection
from numpy import set_printoptions
from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_classif
from sklearn.feature_selection import RFE


#DIR = os.path.dirname(__file__)

# Import dataset:
    
dataset = pd.read_csv("./generated_dataset/gaze_dataset.csv")




# Assign column names to dataset:
#names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'Class']

# Convert dataset to a pandas dataframe:
#dataset = pd.read_csv(url, names=names) 

# Use head() function to return the first 5 rows: 
dataset.head() 
# Assign values to the X and y variables:
X = dataset.iloc[:, 0:64].values
y = dataset.iloc[:, 64].values 

# Split dataset into random train and test subsets:
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.20) 

# Standardize features by removing mean and scaling to unit variance:
scaler = StandardScaler()
scaler.fit(X_train)

X_train = scaler.transform(X_train)
X_test = scaler.transform(X_test) 

#Feature importance

# feature extraction
#Univariate Selection
# feature extraction
pca = PCA(n_components=64)
fit = pca.fit(X)
# summarize components
#print("Explained Variance: %s" % fit.explained_variance_ratio_)
#print(fit.components_)
X_train_features = fit.transform(X_train)
X_test_features = fit.transform(X_test)
# summarize selected features
#print(features[0:5,:])






#SVM Classification

from sklearn.svm import LinearSVC
svm=LinearSVC(C=0.0001)
svm.fit(X_train_features, y_train)
# Predict y data with classifier: 
y_predict = svm.predict(X_test_features)
print("SVM Classification Score Analysis")
svm_accuracy=accuracy_score(y_test, y_predict)
print(svm_accuracy)



# Use the KNN classifier to fit data:
classifier = KNeighborsClassifier(n_neighbors=5)
classifier.fit(X_train_features, y_train) 
# Predict y data with classifier: 
y_predict = classifier.predict(X_test_features)
#print("train shape: " + str(X_train.shape))
#print("score on test: " + str(classifier.score(X_test, y_predict)))
#print("score on train: "+ str(classifier.score(X_train, y_train)))
print("KNN Classification Score Analysis")
knn_accuracy= accuracy_score(y_test, y_predict)
print(knn_accuracy)


#Decision Tree Classifcation

from sklearn.tree import DecisionTreeClassifier
clf = DecisionTreeClassifier()
clf.fit(X_train_features, y_train)
# Predict y data with classifier: 
y_predict = clf.predict(X_test_features)
print("Decision Tree Score Analysis")

decision_tree_accuracy = accuracy_score(y_test, y_predict)

print(decision_tree_accuracy)



# Print results: 
#print(confusion_matrix(y_test, y_predict))
#print(classification_report(y_test, y_predict)) 








