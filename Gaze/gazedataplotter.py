# plots average vs SD of pupil diameter for each user

__author__ = "Shafin Khadem"

import os
import csv
import matplotlib.pyplot as plt

DIR = os.path.dirname(__file__)


with open('generated_dataset/gaze_dataset.csv') as gazedata:
    gazereader = csv.DictReader(gazedata)
    gazes = list(gazereader)

for user in os.scandir(os.path.join(DIR, '../dataset/Finalised')):
    true_avgPupil = []
    true_sdPupil = []
    false_avgPupil = []
    false_sdPupil = []
    for row in gazes:
        if row['user'] != user.name:
            continue
        if row['truth'] == '1':
            true_avgPupil.append(float(row['avg_pupil']))
            true_sdPupil.append(float(row['sd_pupil']))
        else:
            false_avgPupil.append(float(row['avg_pupil']))
            false_sdPupil.append(float(row['sd_pupil']))

    plt.scatter(true_avgPupil, true_sdPupil, c='green')
    plt.scatter(false_avgPupil, false_sdPupil, c='red')
    plt.xlabel('average pupil diameter')
    plt.ylabel('standard deviation of pupil diameter')
    if not os.path.exists('plots'):
        os.makedirs('plots')
    plt.savefig('plots/pupilplot_{}.png'.format(user.name))
    plt.close()
