# Generating dataset for 'Gaze'
#
# Saves processed_data (top 20 valid fixations in terms of duration) in every run
#
#

__author__ = "Washief Hossain"

# native
import os
import csv
import numpy as np
from opengazereader import read_opengaze


DIR = os.path.dirname(__file__)

if not os.path.isdir(os.path.join(DIR, 'generated_dataset')):
    os.mkdir(os.path.join(DIR, 'generated_dataset'))


log = open("log.txt", "w")
data_set = csv.writer(open(os.path.join(
    DIR, "generated_dataset", "gaze_dataset.csv"), "w", newline=''), quoting=csv.QUOTE_NONNUMERIC)

h = []
for i in range(0, 20):
    h.append("sample{}_x".format(i))
    h.append("sample{}_y".format(i))
    h.append("sample{}_d".format(i))
h.append("blinks/duration")
h.append("avg_pupil")
h.append("sd_pupil")
h.append("fixations")
h.append("truth")
h.append("user")
h.append("run")
data_set.writerow(h)

truth_per_directory = {}

with open('../dataset/Annotations.csv', newline='') as annotations:
    reader = csv.DictReader(annotations)
    for row in reader:
        truth_per_directory[row['gaze'][2:-9]] = row['truth']

max_pupil_size_max = 0.0
min_pupil_size_min = 100.0

for user in os.scandir('../dataset/Finalised'):
    log.write(user.name + ": \n")
    for run in os.scandir(user.path):
        log.write(run.name + ": \n")

        with open(os.path.join(run, "Gaze.csv"), newline='') as f:
            reader = csv.reader(f)
            data = list(reader)

        header = data[0]
        data = data[1:]
        duration = float(data[-1][1])
        data = sorted(data, key=lambda entry: float(
            entry[6]), reverse=True)[:20]
        data = sorted(data, key=lambda entry: int(entry[0]))

        # stores a copy of a processed data
        # additional_dir = 'processed_data'
        # additional_dir = os.path.join(run, additional_dir)
        # if not os.path.isdir(additional_dir):
        #     os.mkdir(additional_dir)

        # with open(os.path.join(additional_dir, "output.csv"), "w", newline='') as edited:
        #     writer = csv.writer(edited)
        #     writer.writerow(header)
        #     writer.writerows(data)

        data = [[entry[3], entry[4], entry[6]] for entry in data]
        data = [float(j) for sub in data for j in sub]
        log.write(str(data) + '\n')

        opengazedata = read_opengaze(os.path.join(run, "Gaze.csv"), "nothing")
        blinks = opengazedata[0]['events']['Eblk']
        fixations = opengazedata[0]['events']['Efix']

        pupil_sizes = opengazedata[0]['size']
        standard_deviation_pupil_size = np.std(pupil_sizes)

        max_pupil_size = max(pupil_sizes)
        max_pupil_size_max = max(max_pupil_size_max, max_pupil_size)
        # numpy boolean mask indexing aka fancy indexing
        pupil_sizes = pupil_sizes[pupil_sizes > 10.0]
        min_pupil_size_min = min(min_pupil_size_min, min(pupil_sizes))

        average_pupil_size = np.average(pupil_sizes)

        data.append(len(blinks)/duration)
        data.append(average_pupil_size)
        data.append(standard_deviation_pupil_size)
        data.append(len(fixations))
       # print()
        directory_path= os.path.relpath(run.path,  '../dataset').replace("\\", "/" )     
                                                  
        #data.append(truth_per_directory[os.path.relpath(
         #   run.path,  '../dataset')])
        data.append(truth_per_directory[directory_path])
        data.append(user.name)
        data.append(run.name)
        data_set.writerow(data)

        log.write('standard deviation of pupil size: ' +
                  str(standard_deviation_pupil_size) + '\n')
        log.write('blinks[starttime, endtime, duration]: ' +
                  str(blinks) + '\n')
        log.write('average pupil size: ' + str(average_pupil_size) + '\n')
        log.write('max pupil size: ' + str(max_pupil_size) + '\n\n')

print(max_pupil_size_max, min_pupil_size_min)
