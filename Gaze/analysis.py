# analysis script for natural viewing experiment

__author__ = "Shafin Khadem"

# native
import os

# custom
from constants import *
from opengazereader import read_opengaze
from gazeplotter import draw_fixations, draw_heatmap, draw_scanpath, draw_raw

# external
import numpy
import matplotlib.pyplot as plt


# # # # #
# CONSTANTS
# DIR = os.path.abspath(os.getcwd())
DIR = os.path.dirname(__file__)

for dir in os.scandir(os.path.join(DIR, 'User_7')):
    print(dir.path)

    # read the file
    # opengazedata[0]['time'] = list of timestamps in 0
    # opengazedata[0]['size'] = list of pupil size samples in 0
    opengazedata = read_opengaze(os.path.join(dir, "Gaze.csv"), "nothing")

    # NEW OUTPUT DIRECTORIES
    # create a new output directory for the current participant
    ppname = 'plots'
    pplotdir = os.path.join(dir, ppname)
    # check if the directory already exists
    if not os.path.isdir(pplotdir):
    	# create it if it doesn't yet exist
    	os.mkdir(pplotdir)


    # # # # #
    # PLOTS


    # load blinks, saccades, and fixations
    blinks = opengazedata[0]['events']['Eblk'] # [starttime, endtime, duration]
    saccades = opengazedata[0]['events']['Esac'] # [starttime, endtime, duration, startx, starty, endx, endy]
    fixations = opengazedata[0]['events']['Efix'] # [starttime, endtime, duration, endx, endy]

    print('blinks:', blinks)
    pupilsizes = opengazedata[0]['size']
    maxpupilsize = max(pupilsizes)
    print('max pupil size:', maxpupilsize)
    pupilsizes = pupilsizes[(pupilsizes > 0.0) & (maxpupilsize - pupilsizes < 7)]
    print('standard deviation of pupil size:', numpy.std(pupilsizes))

    print("plotting gaze data")
    # paths
    imagefile = os.path.join(dir, "image.jpg")
    rawplotfile = os.path.join(pplotdir, "raw_data_%s_%d" % (ppname,0))
    scatterfile = os.path.join(pplotdir, "fixations_%s_%d" % (ppname,0))
    scanpathfile =  os.path.join(pplotdir, "scanpath_%s_%d" % (ppname,0))
    heatmapfile = os.path.join(pplotdir, "heatmap_%s_%d" % (ppname,0))

    # raw data points
    plt.close(draw_raw(opengazedata[0]['x'], opengazedata[0]['y'], DISPSIZE, imagefile=imagefile, savefilename=rawplotfile))

    # fixations
    plt.close(draw_fixations(fixations, DISPSIZE, imagefile=imagefile, durationsize=True, durationcolour=False, alpha=0.5, savefilename=scatterfile))

    # scanpath
    plt.close(draw_scanpath(fixations, saccades, DISPSIZE, imagefile=imagefile, alpha=0.5, savefilename=scanpathfile))

    # heatmap
    plt.close(draw_heatmap(fixations, DISPSIZE, imagefile=imagefile, durationweight=True, alpha=0.5, savefilename=heatmapfile))
