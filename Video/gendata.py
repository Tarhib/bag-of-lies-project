import cv2     # for capturing videos
import math   # for mathematical operations
#import matplotlib.pyplot as plt    # for plotting the images %matplotlib inline
#import pandas as pd
#from keras.preprocessing import image   # for preprocessing the images
#import numpy as np    # for mathematical operations
#from keras.utils import np_utils
from skimage.transform import resize   # for resizing images
from pathlib import Path
import os 

def frameExtractFromVideo(videoFilePath,frameFilePath):
    count = 0
    videoFile = videoFilePath;
    cap = cv2.VideoCapture(videoFile)   # capturing the video from the given path
    duration = cap.get(cv2.CAP_PROP_POS_MSEC)
    frameRate = cap.get(5) #frame rate
    x=1
    while(cap.isOpened()):
        frameId = cap.get(1) #current frame number
        ret, frame = cap.read()
        if (ret != True):
            break
        if (frameId % math.floor(frameRate) == 0):
            filename =frameFilePath+"%d.jpg" % count;count+=1
            cv2.imwrite(filename, frame)
    cap.release()
    print ("Done! for video file path"+videoFilePath)


#DIR = Path(__file__).parent
DIR =  os.getcwd()

firstvideoPath = DIR+'/dataset/Finalised/User_0/run_0/video.mp4'
firstvideoFramePath = DIR+'/dataset/Finalised/User_0/run_0/frame'

count = 0
videoFile = firstvideoPath;
cap = cv2.VideoCapture(videoFile)   # capturing the video from the given path
duration = cap.get(cv2.CAP_PROP_POS_MSEC)
frameRate = cap.get(5) #frame rate
x=1
while(cap.isOpened()):
    frameId = cap.get(1) #current frame number
    ret, frame = cap.read()
    if (ret != True):
        break
    if (frameId % math.floor(frameRate) == 0):
        filename =firstvideoFramePath+"%d.jpg" % count;count+=1
        cv2.imwrite(filename, frame)
cap.release()
print ("Done! for video file path"+firstvideoPath)

#frameExtractFromVideo(firstvideoPath,firstvideoFramePath)






