
# BagOfLies
This is a deception detection thesis repository.  
Bag of lies data set can be found [here](https://drive.google.com/file/d/1m0KUbRBRkn-kNpfnHeZwcZ6ie-eOzS71/view).

<details>
<summary>
</em>&nbsp;&nbsp;Password</a>    
</summary>
```
L-1-#$_&L0G123
```
</details>

###  Project Format
```
BagOfLies
	|─── dataset
	|	|───── Annotations.csv
	|	|───── Finalised
	|		|─── ...
	|
	|─── Gaze
	|	|───── generated_dataset
	|	|───── plots
	|	|───── ...
	|
	|─── EEG
	|	|──── generated_dataset
	|	|──── plots
	|	|──── ...
	|
	|─── Audio
	|	|──── ...
	|
	|
	|─── Video
	|	|──── ...
	|
	|─── ...
	|
	|─── README.md
	|─── .gitignore
```
---


### previous works on same dataset

https://ieeexplore.ieee.org/document/9025340

[medium article of another work on same dataset](extra/Deception detection on the Bag-of-lies dataset - by Madis Janno - Medium.mht)
