Possibly useful repos

- https://github.com/vlawhern/arl-eegmodels
- https://github.com/pbashivan/EEGLearn
- https://github.com/SuperBruceJia/EEG-DL
- https://github.com/kylemath/DeepEEG
- https://github.com/mne-tools/mne-python

possibly useful papers

- https://www.researchgate.net/publication/310953136_EEGNet_A_Compact_Convolutional_Network_for_EEG-based_Brain-Computer_Interfaces
- https://www.researchgate.net/publication/316612232_Deception_detection_of_EEG-P300_component_classified_by_SVM_method
- https://journals.plos.org/plosone/article?id=10.1371/journal.pone.0064704
