# plots EEG signals and extract features

__author__ = "Shafin Khadem"

import csv
import numpy as np
from pathlib import Path
import matplotlib.pyplot as plt
import os


DIR = os.path.dirname(__file__)
log = open("log.txt", "w")
if not os.path.isdir(os.path.join(DIR, 'generated_dataset')):
    os.mkdir(os.path.join(DIR, 'generated_dataset'))

#dataset = DIR.joinpath('../dataset')

truth_per_directory = {}
with open('../dataset/Annotations.csv', newline='') as annotations:
    reader = csv.DictReader(annotations)
    for row in reader:
        truth_per_directory[row['gaze'][12:-9]] = row['truth']
# print(truth_per_directory)


channels = ['F3', 'FC5', 'F7', 'T7', 'P7', 'O1',
            'O2', 'P8', 'T8', 'F8', 'AF4', 'FC6', 'F4', ]
data_set = csv.writer(
    open(os.path.join(DIR, "generated_dataset","eeg_dataset.csv"), "w", newline=''), quoting=csv.QUOTE_NONNUMERIC)
data_set.writerow(sum([(f"{channel}_min", f"{channel}_max", f"{channel}_mode",  f"{channel}_median", f"{channel}_mean")
                  for channel in channels], ('truth', 'user', 'run')))


for channel in channels:
    Path(f"plots/normal/{channel}").mkdir(parents=True, exist_ok=True)

def is_number(s):
    try:
        float(s)
        return True
    except (ValueError, TypeError) as e:
        return False


def gen_data(eegdir: Path,usrDir: Path):
    user = usrDir;
    run = eegdir;
    directory_path= os.path.relpath(run,  '../dataset/Finalised').replace("\\", "/" ) 
    #if not eegdir.joinpath('EEG.csv').exists():
     #   print("No EEG.csv in this dir")
      #  return
    with open(os.path.join(run, "EEG.csv"), newline='') as eegdata:
        #truth = truth_per_directory[f"{user}/{run}"]
        truth = truth_per_directory[directory_path]
        eegreader = csv.DictReader(eegdata)
        eegdata = list(eegreader)
        # wanted to use datetime.datetime.fromisoformat(row['Timestamp']), but
        # some timestamps are ill formed :)
        timestamps = [row['Timestamp'] for row in eegdata]
        row = [truth, user, run]
        for channel in channels:
            key = channel+' Value'
            # User_19/run_7 gets a None at the end :)
            vals = [abs(float(row[key])) if is_number(row[key])
                    else 0 for row in eegdata]
            min_val = min(vals)
            max_val = max(vals)
            mean_val = np.average(vals)
            median_val = np.median(vals)
            values, counts = np.unique(vals, return_counts=True)
            mode_val = values[np.argmax(counts)]
            row += [min_val, max_val, mode_val, median_val, mean_val]
            # plotting at most 1000 values
            plt.plot(timestamps[:1000], vals[:1000])
            plt.xlabel('time')
            plt.ylabel(f"voltage of sensors, truth: {truth}")
            plt.xticks([])
            plt.gcf().set_size_inches(50, 6)
           # plt.savefig(f"plots/normal/{channel}/{directory_path}")
            plt.close()
        data_set.writerow(row)


for user in os.scandir('../dataset/Finalised'):
    for rundir in os.scandir(user.path):
            log.write(rundir.name + ": \n")
        #if rundir.joinpath('EEG.csv').exists():
            gen_data(rundir.path,user.path)

# plot(dataset.joinpath('Finalised/User_19/run_7'))
