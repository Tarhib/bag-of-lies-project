# %%
import matplotlib.pyplot as plt

plt.rc('figure', facecolor='white', dpi=300)
plt.rc('font', size=6)

# %%
feature_scores = {  # abootalebi
    'Total\narea': -15.55,
    'A (500-625)': -15.2,
    'Negative\narea': -13.75,
    'Positive\narea': -13.39,
    'A (375-500)': -11.61,
    'Amplitude': -7.71,
    'Absolute\namplitude': -7.66,
    'Median\nfrequency': 6.7,
    'A (125-150)': 5.72,
    'Mode\nfrequency': 5.43,
    'Mean\nfrequency': 5.4,
    'A (625-750)': -5.16
}

feature_scores = {  # gao_fscore_elm
    '$W_{16}$': 0.987,
    '$W_{20}$': 0.987,
    '$W_{11}$': 0.977,
    '$V_{max}$': 0.966,
    '$W_{18}$': 0.953,
    '$W_{17}$': 0.893,
    '$W_{19}$': 0.892,
    '$V_{ptp}$': 0.89,
    '$A_{lf}$': 0.886,
    '$W_{14}$': 0.886,
    '$W_{22}$': 0.881,
    '$W_{15}$': 0.874,
    '$W_{21}$': 0.874,
    '$t_{max}$': 0.581,
    '$W_{12}$': 0.554,
    '$W_{10}$': 0.364,
    '$W_{3}$': 0.317,
    '$f_{mean}$': 0.305,
    '$W_{13}$': 0.213,
    '$R_{L/A}$': 0.162,
    '$W_{7}$': 0.154,
    '$A_{p}$': 0.124,
    '$W_{8}$': 0.097,
    '$W_{1}$': 0.075,
    '$W_{5}$': 0.075,
    '$W_{9}$': 0.069,
    '$V_{min}$': 0.019,
    '$W_{6}$': 0.003,
    '$W_{4}$': 0.002,
    '$W_{2}$': 0.001,
    '$f_{max}$': 0.001,
}

feature_scores = {  # gao_fscore_svm
    '$W_{11}$': 0.977,
    '$W_{20}$': 0.959,
    '$W_{16}$': 0.947,
    '$V_{max}$': 0.937,
    '$W_{18}$': 0.937,
    '$W_{17}$': 0.905,
    '$W_{19}$': 0.881,
    '$V_{ptp}$': 0.877,
    '$A_{lf}$': 0.873,
    '$W_{21}$': 0.871,
    '$W_{22}$': 0.838,
    '$W_{14}$': 0.835,
    '$W_{15}$': 0.82,
    '$t_{max}$': 0.567,
    '$W_{12}$': 0.524,
    '$W_{10}$': 0.381,
    '$f_{mean}$': 0.344,
    '$W_{3}$': 0.311,
    '$A_{p}$': 0.268,
    '$W_{13}$': 0.255,
    '$W_{7}$': 0.184,
    '$R_{L/A}$': 0.162,
    '$W_{8}$': 0.106,
    '$W_{5}$': 0.099,
    '$W_{1}$': 0.085,
    '$W_{9}$': 0.077,
    '$f_{max}$': 0.049,
    '$V_{min}$': 0.019,
    '$W_{4}$': 0.011,
    '$W_{6}$': 0.008,
    '$W_{2}$': 0.001,
}


# function to add value labels
def addlabels(x, y):
    for i in range(len(x)):
        plt.text(i,
                 y[i],
                 y[i],
                 ha='center',
                 va=('top' if y[i] < 0 else 'bottom'),
                 fontsize=4)
        # plt.text(y[i], x[i], y[i]) # for barh


features = list(feature_scores.keys())
scores = list(feature_scores.values())
plt.figure(figsize=(7, 3))
plt.xticks(fontsize=5.5)
plt.axhline(y=0)
plt.bar(features, scores)
addlabels(features, scores)
