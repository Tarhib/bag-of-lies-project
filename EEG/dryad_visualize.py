# %%
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np

plt.rc('figure', facecolor='white', dpi=150)
plt.rc('font', size=12)

# %%
for honesty in ('honest', 'lying'):
    for i in range(1, 16):
        for stimulus in ('probe', 'target', 'irrelevant'):
            print(f"{honesty}/subject{i}/{stimulus}")
            pz = []
            for j in range(1, 6):
                try:
                    session = pd.read_csv(
                        f"../dataset/dryad/{honesty}/subject{i}/session{j}/{stimulus}/{stimulus}.txt",
                        sep='\t',
                        header=None).T.dropna()
                    trial_count = len(session.values) // 800
                    single_trials = np.array(
                        np.split(session.values, trial_count))
                    trials_flat = single_trials[:, :, 12:13].reshape(
                        trial_count, -1)
                    single_trials = single_trials[
                        (trials_flat.min(axis=1) >= -75)
                        & (trials_flat.max(axis=1) <= 75)]
                    single_trials_pz = single_trials[:, :, 9]
                    baseline_corrected_pz = (
                        single_trials_pz -
                        np.mean(single_trials_pz[:, :250], axis=1).reshape(
                            -1, 1)).flatten()
                    pz.append(baseline_corrected_pz)
                except Exception as e:
                    print(e)
            if pz:
                min_session = min([len(session) for session in pz])
                pz = np.mean([session[:min_session] for session in pz], axis=0)
                print(pz.shape)
            np.savetxt(
                f"../dataset/dryad/{honesty}/subject{i}/average_corrected_{stimulus}.txt",
                pz,
                delimiter=',')

# %%
max_amp_honest = []
for i in range(1, 16):
    print(f"honest/subject{i}")
    honest_pz = np.loadtxt(
        f"../dataset/dryad/honest/subject{i}/average_corrected_probe.txt",
        delimiter=',')
    if honest_pz.size == 0:
        continue
    single_trials = np.array(np.split(honest_pz,
                                      len(honest_pz) // 800))[:, 350:550]
    max_amp_honest += np.max(single_trials, axis=1).flatten().tolist()
len(max_amp_honest)

# %%
max_amp_lying = []
for i in range(1, 16):
    print(f"lying/subject{i}")
    lying_pz = np.loadtxt(
        f"../dataset/dryad/lying/subject{i}/average_corrected_probe.txt",
        delimiter=',')
    if lying_pz.size == 0:
        continue
    single_trials = np.array(np.split(lying_pz,
                                      len(lying_pz) // 800))[:, 350:550]
    max_amp_lying += np.max(single_trials, axis=1).flatten().tolist()
len(max_amp_lying)

# %%
df = pd.DataFrame(np.vstack(
    (np.column_stack((max_amp_honest, ['Honest'] * len(max_amp_honest))),
     np.column_stack((max_amp_lying, ['Lying'] * len(max_amp_lying))))),
                  columns=['max_amplitude', 'honesty'])
df['max_amplitude'] = pd.to_numeric(df['max_amplitude'])
sns.boxplot(x="honesty", y="max_amplitude", data=df)


# %%
def average_trial(joined_data):
    length = len(joined_data)
    return np.mean(np.array(np.split(joined_data, length // 800)), axis=0)


# %%
honest_pz_probe = []
for i in range(1, 16):
    print(f"honest/subject{i}")
    average_corrected_probe = np.loadtxt(
        f"../dataset/dryad/honest/subject{i}/average_corrected_probe.txt",
        delimiter=',')
    honest_pz_probe.append(average_trial(average_corrected_probe))
honest_pz_probe = np.mean(honest_pz_probe, axis=0)

# %%
lying_pz_probe = []
for i in range(1, 16):
    print(f"lying/subject{i}")
    average_corrected_probe = np.loadtxt(
        f"../dataset/dryad/lying/subject{i}/average_corrected_probe.txt",
        delimiter=',')
    if len(average_corrected_probe):
        lying_pz_probe.append(average_trial(average_corrected_probe))
lying_pz_probe = np.mean(lying_pz, axis=0)

# %%
honest_pz_probe = average_trial(
    np.loadtxt('../dataset/dryad/honest/subject14/average_corrected_probe.txt',
               delimiter=','))

honest_pz_irrelevant = average_trial(
    np.loadtxt(
        '../dataset/dryad/honest/subject14/average_corrected_irrelevant.txt',
        delimiter=','))

honest_pz_target = average_trial(
    np.loadtxt(
        '../dataset/dryad/honest/subject14/average_corrected_target.txt',
        delimiter=','))

lying_pz_probe = average_trial(
    np.loadtxt('../dataset/dryad/lying/subject4/average_corrected_probe.txt',
               delimiter=','))

lying_pz_irrelevant = average_trial(
    np.loadtxt(
        '../dataset/dryad/lying/subject4/average_corrected_irrelevant.txt',
        delimiter=','))

lying_pz_target = average_trial(
    np.loadtxt('../dataset/dryad/lying/subject4/average_corrected_target.txt',
               delimiter=','))

plt.figure(figsize=(14, 6))
plt.plot(range(-500, 1100, 2), honest_pz_probe, 'g', label='honest_probe')
plt.plot(range(-500, 1100, 2),
         honest_pz_irrelevant,
         'g:',
         label='honest_irrelevant')
plt.plot(range(-500, 1100, 2), honest_pz_target, 'g--', label='honest_target')
plt.plot(range(-500, 1100, 2), lying_pz_probe, 'r', label='lying_probe')
plt.plot(range(-500, 1100, 2),
         lying_pz_irrelevant,
         'r:',
         label='lying_irrelevant')
plt.plot(range(-500, 1100, 2), lying_pz_target, 'r--', label='lying_target')
plt.xlabel('Miliseconds from stimulus')
plt.ylabel(r'Amplitude $\mu V$')
plt.legend()
