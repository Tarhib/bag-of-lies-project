# Bar plot of overall dataset

import os
import csv
import matplotlib.pyplot as plt
from pathlib import Path
import numpy as np

DIR = Path(__file__).parent


with open('generated_dataset/eeg_dataset.csv') as eegdata:
    eegreader = csv.DictReader(eegdata, quoting=csv.QUOTE_NONNUMERIC)
    eegs = list(eegreader)


true_samples = [row for row in eegs if row['truth'] == '1']
false_samples = [row for row in eegs if row['truth'] != '1']

channels = ['F3', 'FC5', 'F7', 'T7', 'P7', 'O1',
            'O2', 'P8', 'T8', 'F8', 'AF4', 'FC6', 'F4', ]
attributes = sum([(f"{channel}_min", f"{channel}_max", f"{channel}_mode", f"{channel}_median", f"{channel}_mean")
                  for channel in channels], ())


def centroid(data_set):
    ret = {}
    for attr in attributes:
        ret[attr] = np.average([row[attr] for row in data_set])
    return ret


def data_tuple(data):
    return tuple([data[attribute] for attribute in attributes])


def distance(point1, point2):
    return np.linalg.norm(np.subtract(data_tuple(point1), data_tuple(point2)))


true_centroid = centroid(true_samples)
false_centroid = centroid(false_samples)


true_samples_filtered = [row for row in true_samples if distance(
    row, true_centroid) < distance(row, false_centroid)]

false_samples_filtered = [row for row in false_samples if distance(
    row, true_centroid) > distance(row, false_centroid)]


promising_true = csv.writer(
    open(DIR.joinpath("generated_dataset/promising_true.csv"), "w", newline=''), quoting=csv.QUOTE_NONNUMERIC)
for samples in true_samples_filtered:
    promising_true.writerow([samples['user'], samples['run']])

promising_false = csv.writer(
    open(DIR.joinpath("generated_dataset/promising_false.csv"), "w", newline=''), quoting=csv.QUOTE_NONNUMERIC)
for samples in false_samples_filtered:
    promising_false.writerow([samples['user'], samples['run']])


def plot_bar(attribute):
    true_attribute_samples = [row[attribute]
                              for row in true_samples_filtered]
    false_attribute_samples = [row[attribute]
                               for row in false_samples_filtered]

    true_average = np.average(true_attribute_samples)
    false_average = np.average(false_attribute_samples)

    plt.bar(range(0, len(true_attribute_samples)),
            true_attribute_samples, color='green', label='true')
    plt.bar(range(len(true_attribute_samples), len(true_attribute_samples)+len(false_attribute_samples)),
            false_attribute_samples, color='red', label='false')
    plt.plot(range(0, len(true_attribute_samples)), [
             true_average]*len(true_attribute_samples), color='black')
    plt.plot(range(len(true_attribute_samples), len(true_attribute_samples)+len(false_attribute_samples)),
             [false_average]*len(false_attribute_samples), color='black', label='average')
    plt.title(attribute.replace('_', ' '))
    plt.ylabel(attribute.replace('_', ' '))
    plt.xlabel('samples')
    plt.xticks([])
    plt.legend(prop={'size': 6})
    if not os.path.exists('plots'):
        os.makedirs('plots')
    plt.savefig(
        'plots/bar_{}.png'.format(attribute.replace('/', '_by_')))
    plt.close()


for attribute in attributes:
    plot_bar(attribute)
