import os
import csv
import numpy as np
from moviepy.editor import *
import matplotlib.pyplot as plt
import librosa
import librosa.display

DIR = os.path.dirname(__file__)
# print(DIR)

if not os.path.isdir(os.path.join(DIR, 'generated_dataset')):
    os.mkdir(os.path.join(DIR, 'generated_dataset'))

if not os.path.isdir(os.path.join(DIR, 'plots')):
    os.mkdir(os.path.join(DIR, 'plots'))


log = open("log.txt", "w")
data_set = csv.writer(open(os.path.join(
    DIR, "generated_dataset", "audio_dataset.csv"), "w", newline=''), quoting=csv.QUOTE_NONNUMERIC)

truth_per_directory = {}
with open('../dataset/Annotations.csv', newline='') as annotations:
    reader = csv.DictReader(annotations)
    for row in reader:
        # if there is video, there is gaze
        truth_per_directory[row['gaze'][2:-9]] = row['truth']


"""
This code segment is very slow.
Extracts the audio from the video and saves it in the same directory.
Save waveforms of the audio in plots/waveforms/0 and plots/waveforms/1
"""


def audioExtract():

    if not os.path.isdir(os.path.join(DIR, 'plots/waveforms')):
        os.mkdir(os.path.join(DIR, 'plots/waveforms'))

    if not os.path.isdir(os.path.join(DIR, 'plots/waveforms/0')):
        os.mkdir(os.path.join(DIR, 'plots/waveforms/0'))

    if not os.path.isdir(os.path.join(DIR, 'plots/waveforms/1')):
        os.mkdir(os.path.join(DIR, 'plots/waveforms/1'))

    for user in os.scandir('../dataset/Finalised'):
        log.write(user.name + ": \n")
        for run in os.scandir(user.path):
            log.write(run.name + ": \n")
            videoclip = VideoFileClip(os.path.join(run, "video.mp4"))
            audioclip = videoclip.audio
            audioclip.write_audiofile(os.path.join(run, "audio.mp3"))
            audioclip.close()
            videoclip.close()
            audio, sr = librosa.load(os.path.join(run, "audio.mp3"))
            plt.figure(figsize=(12, 4))
            librosa.display.waveshow(audio, sr=sr)
            plt.title(f"{user.name}_{run.name}")
            plt.ylim([-.5, .5])
            plt.savefig(
                f"plots/waveforms/{truth_per_directory[f'Finalised/{user.name}/{run.name}']}/{user.name}_{run.name}.png")
            plt.close()


# audioExtract()

features = ["amp_env", "rms", "zcr", "spectral_centroid",
            "spectral_bandwidth", "spectral_rolloff", "chroma_freq", "mfcc"]

for feature in features:
    if not os.path.isdir(os.path.join(DIR, f'plots/{feature}')):
        os.mkdir(os.path.join(DIR, f'plots/{feature}'))


FRAME_SIZE = 1024
HOP_LENGTH = 512


"""Librosa does not have built in amplitude emvelop feature extraction."""


def amplitude_envelope(signal, frame_size, hop_length):
    return np.array([max(signal[i:i+frame_size]) for i in range(0, len(signal), hop_length)])


for user in os.scandir('../dataset/Finalised'):
    log.write(user.name + ": \n")
    for run in os.scandir(user.path):
        log.write(run.name + ": \n")
        time_dom, sr = librosa.load(os.path.join(run, "audio.mp3"))
        freq_dom = np.abs(librosa.stft(
            time_dom, n_fft=FRAME_SIZE, hop_length=HOP_LENGTH))

        data = {}

        data["amp_env"] = amplitude_envelope(time_dom, FRAME_SIZE, HOP_LENGTH)
        data["rms"] = librosa.feature.rms(
            time_dom, frame_length=FRAME_SIZE, hop_length=HOP_LENGTH)[0]
        data["zcr"] = librosa.feature.zero_crossing_rate(
            time_dom, frame_length=FRAME_SIZE, hop_length=HOP_LENGTH)[0]
        data["spectral_centroid"] = librosa.feature.spectral_centroid(S=freq_dom)[
            0]
        data["spectral_bandwidth"] = librosa.feature.spectral_bandwidth(S=freq_dom)[
            0]
        data["spectral_rolloff"] = librosa.feature.spectral_rolloff(S=freq_dom)[
            0]
        data["chroma_freq"] = librosa.feature.chroma_cqt(time_dom)[0]
        data["mfcc"] = librosa.feature.mfcc(time_dom, sr)[0]
        # print(amp_env.shape, rms.shape, zcr.shape, spectral_centroid.shape, spectral_bandwidth.shape, spectral_rolloff.shape, chroma_freq.shape, mfcc.shape)
        for feature in features:
            plt.bar(range(0, len(data[feature])), data[feature],
                    color="green" if truth_per_directory[f'Finalised/{user.name}/{run.name}'] == '1' else "red")
            plt.title(f"{user.name}_{run.name}")
            plt.xticks([])
            plt.savefig('plots/' + feature + '/' +
                        '{}_{}.png'.format(user.name, run.name))
            plt.close()
